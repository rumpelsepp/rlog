// Package rlog provides a partial reimplementation of the log package in the
// standard library. This package provides simple logging with loglevels and an
// interface quite similar to the standard library. The following environment
// variables are supported:
//
// GO_RLOG_LEVEL
// GO_RLOG_JOURNAL
//
// In addition to the SetLogLevel() methods, the loglevel can be set globally
// via an environment variable. The recognized values are: CRIT, ERR, WARN,
// NOTICE, INFO, and DEBUG. These constants are defined in RFC5435,
// Section 6.2.1.
//
// For your convenience, there is a package level logger available, which can be
// accessed using the relevant methods. The package level logger is there for
// programs that do not need several loggers. It aims to simplify this
// particular usecase. If you need several loggers, please instantiate them as
// needed and do not use the package level logger.
//
// Timestamps are not supported by design, since this is almost always not
// needed. On the terminal, you can pipe the output through ts(1) from
// moreutils. Using a logging service, the timestamps are already handled
// perfectly. This avoids the annoying problem of doubled timestamps in the log
// database, such as: Aug 10 07:24:37 host service[pid]: TIMESTAMP MESSAGE.
package rlog

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"runtime"
	"runtime/debug"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
)

// RFC5424 Section 6.2.1
const (
	EMERG = iota
	ALERT
	CRIT
	ERR
	WARNING
	NOTICE
	INFO
	DEBUG
)

var priostr = map[int]string{
	EMERG:   "", // unused
	ALERT:   "", // unused
	CRIT:    "[!]",
	ERR:     "[E]",
	WARNING: "[W]",
	NOTICE:  "[n]",
	INFO:    "[i]",
	DEBUG:   "[d]",
}

// DefaultLogLevel holds the default loglevel of this package.
// It is exported in order to be overwritable at compile time
// by -ldflags="-X ...".
var DefaultLogLevel = INFO

// Logger is the handle to be used for emitting logmessages.
// Do not instantiate it manually, use NewLogger().
type Logger struct {
	writer     io.Writer
	buf        bytes.Buffer
	logLevel   int32
	module     string
	prefix     bool
	journal    bool
	lines      int
	stacktrace bool
	mu         sync.Mutex
}

// NewLogger creates a fresh instance if Logger. The environment variable
// GO_RLOG_LEVEL is checked whether it is equal to the strings EMERG, ALERT,
// CRIT, ERR, WARNING, INFO, DEBUG. In this case, the loglevel of the returned
// Logger is set accordingly. Otherwise, DefaultLogLevel is used.  Logger by
// default logs to stderr.
func NewLogger(w io.Writer) *Logger {
	logLevel := DefaultLogLevel

	if level, ok := os.LookupEnv("GO_RLOG_LEVEL"); ok {
		switch strings.ToLower(level) {
		// The remaining three are unused, as such priorities
		// belong to the kernel.
		case "crit":
			logLevel = CRIT
		case "err":
			logLevel = ERR
		case "warning":
			logLevel = WARNING
		case "notice":
			logLevel = NOTICE
		case "info":
			logLevel = INFO
		case "debug":
			logLevel = DEBUG
		default:
			fmt.Fprintf(os.Stderr, "rlog: unrecognized loglevel: %s\n", level)
		}
	}

	_, journal := os.LookupEnv("GO_RLOG_JOURNAL")
	_, stacktrace := os.LookupEnv("GO_RLOG_STACKTRACE")

	var calldepth int
	if rawCalldepth, ok := os.LookupEnv("GO_RLOG_LINES"); ok {
		if c, err := strconv.ParseInt(rawCalldepth, 10, 32); err == nil {
			calldepth = int(c)
		}
	}

	return &Logger{
		writer:     w,
		logLevel:   int32(logLevel),
		prefix:     true,
		journal:    journal,
		stacktrace: stacktrace,
		lines:      calldepth,
	}
}

// SetLogLevel sets the loglevel of the logger. Please use the
// exported constants to set the loglevel accordingly.
func (l *Logger) SetLogLevel(logLevel int) {
	atomic.StoreInt32(&l.logLevel, int32(logLevel))
}

// GetLogLevel retrieves the loglevel from the logger.
func (l *Logger) GetLogLevel() int {
	return int(atomic.LoadInt32(&l.logLevel))
}

// SetModule sets the prefix of the logger.
func (l *Logger) SetModule(module string) {
	l.mu.Lock()
	l.module = module
	l.mu.Unlock()
}

// SetWriter sets the output destination for the logger.
func (l *Logger) SetWriter(writer io.Writer) {
	l.mu.Lock()
	l.writer = writer
	l.mu.Unlock()
}

// EnablePrefix enables or disables the logging prefix, for instance "[d]".
func (l *Logger) EnablePrefix(enabled bool) {
	l.mu.Lock()
	l.prefix = enabled
	l.mu.Unlock()
}

// EnableJournalPrio enables priority prefixes like <1>. If logging
// to systemd-journal(8), the priority is parsed and stored
// along the message.
func (l *Logger) EnableJournalPrio(enabled bool) {
	l.mu.Lock()
	l.journal = enabled
	l.mu.Unlock()
}

// EnableLines enables file names and line numbers in the log output.
func (l *Logger) EnableLines(calldepth int) {
	l.mu.Lock()
	l.lines = calldepth
	l.mu.Unlock()
}

// EnableStacktrace enables printing a stacktrace for ERR and CRIT messages.
func (l *Logger) EnableStacktrace(enabled bool) {
	l.mu.Lock()
	l.stacktrace = enabled
	l.mu.Unlock()
}

func (l *Logger) output(prio int, s string) {
	l.mu.Lock()
	l.buf.Reset()

	for _, line := range strings.Split(s, "\n") {
		if l.journal {
			l.buf.WriteString(fmt.Sprintf("<%d>", prio))
		}

		if l.module != "" && l.prefix {
			l.buf.WriteString(l.module)
			l.buf.WriteString(": ")
		}

		if l.prefix {
			l.buf.WriteString(priostr[prio])
			l.buf.WriteString(" ")
		}

		if l.lines > 0 {
			// Release lock while getting caller info — it's expensive.
			l.mu.Unlock()
			_, file, line, ok := runtime.Caller(l.lines)
			if !ok {
				file = "???"
				line = 0
			}
			l.mu.Lock()
			l.buf.WriteString(file)
			l.buf.WriteString(":")
			l.buf.WriteString(fmt.Sprintf("%d", line))
			l.buf.WriteString(": ")
		}

		l.buf.WriteString(line)
		l.buf.WriteString("\n")
		l.buf.WriteTo(l.writer)

		if l.stacktrace && prio <= ERR {
			stacktrace := debug.Stack()
			io.Copy(l.writer, bytes.NewReader(stacktrace))
		}
	}

	l.mu.Unlock()
}

// Debug logs a message with priority debug. A newline is appended.
func (l *Logger) Debug(v ...interface{}) {
	if l.GetLogLevel() >= DEBUG {
		l.output(DEBUG, fmt.Sprint(v...))
	}
}

// Debugln is the same as Debug.
func (l *Logger) Debugln(v ...interface{}) {
	if l.GetLogLevel() >= DEBUG {
		l.output(DEBUG, fmt.Sprint(v...))
	}
}

// Debugf logs a message wit priority debug. It accepts a Printf style format string.
func (l *Logger) Debugf(format string, v ...interface{}) {
	if l.GetLogLevel() >= DEBUG {
		l.output(DEBUG, fmt.Sprintf(format, v...))
	}
}

// Info logs a message with priority info. A newline is appended.
func (l *Logger) Info(v ...interface{}) {
	if l.GetLogLevel() >= INFO {
		l.output(INFO, fmt.Sprint(v...))
	}
}

// Infoln is the same as Infoln.
func (l *Logger) Infoln(v ...interface{}) {
	if l.GetLogLevel() >= INFO {
		l.output(INFO, fmt.Sprint(v...))
	}
}

// Infof logs a message wit priority info. It accepts a Printf style format string.
func (l *Logger) Infof(format string, v ...interface{}) {
	if l.GetLogLevel() >= INFO {
		l.output(INFO, fmt.Sprintf(format, v...))
	}
}

// Notice logs a message with priority notice. A newline is appended.
func (l *Logger) Notice(v ...interface{}) {
	if l.GetLogLevel() >= NOTICE {
		l.output(NOTICE, fmt.Sprint(v...))
	}
}

// Noticeln is the same as Notice.
func (l *Logger) Noticeln(v ...interface{}) {
	if l.GetLogLevel() >= NOTICE {
		l.output(NOTICE, fmt.Sprint(v...))
	}
}

// Noticef logs a message wit priority notice. It accepts a Printf style format string.
func (l *Logger) Noticef(format string, v ...interface{}) {
	if l.GetLogLevel() >= NOTICE {
		l.output(NOTICE, fmt.Sprintf(format, v...))
	}
}

// Warning logs a message with priority warning. A newline is appended.
func (l *Logger) Warning(v ...interface{}) {
	if l.GetLogLevel() >= WARNING {
		l.output(WARNING, fmt.Sprint(v...))
	}
}

// Warningln is the same as Warning.
func (l *Logger) Warningln(v ...interface{}) {
	if l.GetLogLevel() >= WARNING {
		l.output(WARNING, fmt.Sprint(v...))
	}
}

// Warningf logs a message wit priority warning. It accepts a Printf style format string.
func (l *Logger) Warningf(format string, v ...interface{}) {
	if l.GetLogLevel() >= WARNING {
		l.output(WARNING, fmt.Sprintf(format, v...))
	}
}

// Err logs a message with loglevel err. A newline is appended.
func (l *Logger) Err(v ...interface{}) {
	if l.GetLogLevel() >= ERR {
		l.output(ERR, fmt.Sprint(v...))
	}
}

// Errln is the same as Err.
func (l *Logger) Errln(v ...interface{}) {
	if l.GetLogLevel() >= ERR {
		l.output(ERR, fmt.Sprint(v...))
	}
}

// Errf logs a message wit loglevel err. It accepts a Printf style format string.
func (l *Logger) Errf(format string, v ...interface{}) {
	if l.GetLogLevel() >= ERR {
		l.output(ERR, fmt.Sprintf(format, v...))
	}
}

// Crit logs a message and terminates the program with exit code 1.
func (l *Logger) Crit(v ...interface{}) {
	if l.GetLogLevel() >= CRIT {
		l.output(CRIT, fmt.Sprint(v...))
	}
	os.Exit(1)
}

// Critln is the same as Crit.
func (l *Logger) Critln(v ...interface{}) {
	if l.GetLogLevel() >= CRIT {
		l.output(CRIT, fmt.Sprint(v...))
	}
	os.Exit(1)
}

// Critf logs a message and terminates the program with exit code 1.
// It accepts a Printf style format string.
func (l *Logger) Critf(format string, v ...interface{}) {
	if l.GetLogLevel() >= CRIT {
		l.output(CRIT, fmt.Sprintf(format, v...))
	}
	os.Exit(1)
}

// Create a package level logger, such that it is possible to do:
// log.Debug("Hi") from outside this package. For the lazy people. :)
var std = NewLogger(os.Stderr)

// SetLogLevel is the same as SetLogLevel, but for the package level
// default logger.
func SetLogLevel(logLevel int) {
	std.SetLogLevel(logLevel)
}

// GetLogLevel is the same as GetLogLevel, but for the package level
// default logger.
func GetLogLevel() int {
	return std.GetLogLevel()
}

// SetModule is the same as SetModule, but for the package level
// default logger.
func SetModule(module string) {
	std.SetModule(module)
}

// SetWriter is the same as SetWriter, but for the package level
// default logger.
func SetWriter(writer io.Writer) {
	std.SetWriter(writer)
}

// EnablePrefix is the same as EnablePrefix, but for the package level
// default logger.
func EnablePrefix(enabled bool) {
	std.EnablePrefix(enabled)
}

// EnableJournalPrio is the same as EnableJournalPrio, but for the package level
// default logger.
func EnableJournalPrio(enabled bool) {
	std.EnableJournalPrio(enabled)
}

// EnableLines is the same as EnableLines, but for the package level
// default logger.
func EnableLines(calldepth int) {
	std.EnableLines(calldepth)
}

// EnableStacktrace is the same as EnableLines, but for the package level
// default logger.
func EnableStacktrace(enabled bool) {
	std.EnableStacktrace(enabled)
}

// Debug is the same as Debug, but for the package level
// default logger.
func Debug(v ...interface{}) {
	std.Debug(v...)
}

// Debugln is the same as Debugln, but for the package level
// default logger.
func Debugln(v ...interface{}) {
	std.Debugln(v...)
}

// Debugf is the same as Debugf, but for the package level
// default logger.
func Debugf(format string, v ...interface{}) {
	std.Debugf(format, v...)
}

// Info is the same as Info, but for the package level
// default logger.
func Info(v ...interface{}) {
	std.Info(v...)
}

// Infoln is the same as Infoln, but for the package level
// default logger.
func Infoln(v ...interface{}) {
	std.Infoln(v...)
}

// Infof is the same as Infof, but for the package level
// default logger.
func Infof(format string, v ...interface{}) {
	std.Infof(format, v...)
}

// Notice is the same as Notice, but for the package level
// default logger.
func Notice(v ...interface{}) {
	std.Notice(v...)
}

// Noticeln is the same as Noticeln, but for the package level
// default logger.
func Noticeln(v ...interface{}) {
	std.Noticeln(v...)
}

// Noticef is the same as Noticef, but for the package level
// default logger.
func Noticef(format string, v ...interface{}) {
	std.Noticef(format, v...)
}

// Warning is the same as Warning, but for the package level
// default logger.
func Warning(v ...interface{}) {
	std.Warning(v...)
}

// Warningln is the same as Warningln, but for the package level
// default logger.
func Warningln(v ...interface{}) {
	std.Warningln(v...)
}

// Warningf is the same as Warningf, but for the package level
// default logger.
func Warningf(format string, v ...interface{}) {
	std.Warningf(format, v...)
}

// Err is the same as Err, but for the package level
// default logger.
func Err(v ...interface{}) {
	std.Err(v...)
}

// Errln is the same as Errln, but for the package level
// default logger.
func Errln(v ...interface{}) {
	std.Errln(v...)
}

// Errf is the same as Errf, but for the package level
// default logger.
func Errf(format string, v ...interface{}) {
	std.Errf(format, v...)
}

// Crit is the same as Crit, but for the package level
// default logger.
func Crit(v ...interface{}) {
	std.Crit(v...)
}

// Critln is the same as Critln, but for the package level
// default logger.
func Critln(v ...interface{}) {
	std.Critln(v...)
}

// Critf is the same as Critf, but for the package level
// default logger.
func Critf(format string, v ...interface{}) {
	std.Critf(format, v...)
}
