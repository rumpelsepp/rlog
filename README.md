# rlog

[![GoDoc](https://godoc.org/git.sr.ht/~rumpelsepp/rlog?status.svg)](https://godoc.org/git.sr.ht/~rumpelsepp/rlog)

Package rlog provides a reimplementation of the log package in the
standard library. This package provides simple logging with loglevels and an
interface quite similar to the standard library. Supported loglevels are
equivalent to the syslog standard, RFC5424.

## Quickstart

```go
package main

import "git.sr.ht/~rumpelsepp/rlog"

func main() {
    rlog.SetLogLevel(log.DEBUG)
    rlog.Debugln("debug message")
    rlog.Infoln("info message")
    rlog.Noticeln("info message")
    rlog.Warnln("warn message")
    rlog.Errln("error message")
    rlog.Critln("critical message, which exits the application")
}
```
